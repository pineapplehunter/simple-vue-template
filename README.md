# simple-vue

a simplified (just deleted a lot of files) version of the webpack template.

# Write
start by cloning the repo.

```bash
git clone https://github.com/peshogo/simple-vue-template.git
```

the only file youshould look at is `src/App.vue`.

```html
<template>
  <div>
    <h1>{{ hello }}</h1>
  </div>
</template>

<script>
export default {
  name: 'App',
  data: function () {
    return {
      hello: "Hello World!"
    }
  }
}
</script>

<style>
</style>
```

you can do any vue things here.

# Run
start it with a simple

```bash
npm run dev
# OR
# npm run start
# it's doing the same thing!
```

# production
I don't think you would want to use this for production, but just in case.
You can run this to generate the static files into `dist`.

```bash
npm run build
```

